//
//  MKAppGroups.swift
//  MKKit
//
//  Created by Lachlan Grant on 4/10/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

public struct MKAppGroups {
	public static let MKKit = "group.com.lachlangrant.MKKit"
	public static let General = "group.com.lachlangrant.General"
    public static let Reviews = "group.com.lachlangrant.Reviews"
	
	public static let ConvertNowData = "group.com.lachlangrant.currency.data"
	public static let ConvertNowSettings = "group.com.lachlangrant.currency.settings"
}
