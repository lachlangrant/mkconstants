//
//  MKBundleID.swift
//  MKKit
//
//  Created by Lachlan Grant on 22/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

public struct MKBundleID {
	public static let ConvertNow = "com.lachlangrant.currency"
	public static let PersonalBest = "com.lachlangrant.PersonalBest"
	public static let MKKit = "com.lachlangrant.MKKit"
	public static let ConvertKit = "com.lachlangrant.ConvertKit"
	public static let MKUtilityKit = "com.lachlangrant.MKUtilityKit"
	public static let MKStore = "com.lachlangrant.MKStore"
	public static let MKUIKit = "com.lachlangrant.MKUIKit"
}
