//
//  MKStoreProductID.swift
//  MKKit
//
//  Created by Lachlan Grant on 4/10/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation

public struct MKStoreProductID {
	public static let ConvertNowCrypto = "com.lachlangrant.currency.purchase.crypto"
	public static let ConvertNowUnlimActive = "com.lachlangrant.currency.purchase.unlimactive"
	public static let ConvertNowPro = "com.lachlangrant.currency.purchase.pro"
}
