Pod::Spec.new do |s|
  s.name             = 'MKConstants'
  s.version          = '0.1.5'
  s.summary          = 'Pod for storing Constants'
  s.description      = <<-DESC
Pod Contains constants for App Groups, Bundle IDs, Cloud Kit Containers and IAP Product Ids
                       DESC
  s.homepage         = 'https://bitbucket.org/lachlangrant/MKConstants'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Lachlan Grant' => 'lachlangrant@rbvea.co' }
  s.source           = { :git => 'https://lachlangrant@bitbucket.org/lachlangrant/MKConstants.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.source_files = 'MKConstants/Classes/**/*'
end
